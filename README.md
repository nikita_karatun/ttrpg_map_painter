# TTRPG Map Painer

Simple application to paint the table top RPG maps.

Although for now it is more for sketching since actual paining functionality is not implemented yet.

But since textures and tiles are in future plans the name *painter* chosen instead of a *sketcher*.

![the_app_in_action](readme/ttrpg_map_painter.png)

The application follows rather simplified approach for the map design and meant for the quick TTRPG map creation, perhaps even in the middle of the game session.

* Main working area is a infinite canvas with the pre-drawn grid.
* The *walls* may be drawn over the grid lines, that is only horizontally or vertically.
* Second (and as for now last) instrument, the eraser, can be used to remove walls, e.g. for the entrances.
* The canvas may be travelled hence and forth by dragging it with the middle mouse button. There are no scrollbars but handy arrow will show the lost traveller where the drawn part of the map is if she travelled too far away from it.
* The map may be exported to one of the supported raster image formats (JPEG, PNG, BMP) or to SVG if you are more into the vectors.
* The exported map may be printed and used offline or uploaded to online TTRPG web application, the later is my case, I use it on [roll20](https://roll20.net).

## Future plans

* Rectangular eraser.
* Walls selection, copy-paste, cut-paste.
* Tiles, textures, walls styles.
