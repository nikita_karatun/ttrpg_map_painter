#pragma once

#include <memory>
#include <set>
#include <sstream>

#include <QJsonDocument>
#include <QImage>

#include "map.h"
#include "painting.h"

class QSvgGenerator;

namespace Ttrpg_map_painter {

    namespace Map_io {

        QJsonDocument to_json(const Map& map);
        Map from_json(const QJsonDocument& doc);

        std::unique_ptr<Standard_map_painter> create_image_painter(const Settings&, const Map&); // TODO move to painting?

        QImage create_image(Standard_map_painter *painter, bool no_bg_supported);
        void write(Standard_map_painter *map_painter, QSvgGenerator& svg);

        struct Supported_file_types {
            public:
                struct Group {
                    private:
                        std::string name_;
                        std::vector<std::string> extenstions_;
                    public:
                        Group(const std::string& name, std::initializer_list<std::string> es)
                            :name_(name), extenstions_(es)
                        {}
                        void print(std::ostream& stream) const;
                        auto cbegin() const { return extenstions_.cbegin(); }
                        auto cend() const { return extenstions_.cend(); }
                };
            private:
                std::vector<Group> groups_;
                const QString file_dialog_filter_str_;
                std::set<QString> extensions_set_;

                static QString to_file_dialog_filter_str(const std::vector<Group>& groups);
                static std::set<QString> to_extensions_set(const std::vector<Group>& groups);
            public:
                Supported_file_types(std::initializer_list<Group> list) 
                    :groups_(list),
                    file_dialog_filter_str_(to_file_dialog_filter_str(groups_)),
                    extensions_set_(to_extensions_set(groups_)) {}
                const QString& file_dialog_filter_str() const { return file_dialog_filter_str_; }
                bool contains(const QString& ext) const { return extensions_set_.find(ext) != extensions_set_.cend(); }
        };

    }

}
