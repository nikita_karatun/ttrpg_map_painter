#include "settings_dialog.h"

#include <iostream>

#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QGroupBox>

#include <QLabel>
#include <QSpinBox>
#include <QCheckBox>
#include <QDialogButtonBox>

#include "context.h"

namespace Ttrpg_map_painter {

    Settings_dialog::Settings_dialog(QWidget *parent, Application_context& context) :QDialog(parent), context_(context) {

        setWindowTitle("Settings");

        auto group = new QGroupBox("Export settings", this);

        auto& export_settings = context_.settings().export_settings();

        cell_size_input_ = new QSpinBox(this);
        cell_size_input_->setRange(10, 200);
        cell_size_input_->setValue(export_settings.cell_size());

        offset_input_ = new QSpinBox(this);
        offset_input_->setRange(0, 1000);
        offset_input_->setValue(export_settings.offset());

        background_check_box_ = new QCheckBox(this);
        background_check_box_->setCheckState(export_settings.bg_enabled() ? Qt::Checked : Qt::Unchecked);

        auto bg_check_box_with_desc = new QWidget;
        auto check_box_layout = new QVBoxLayout(bg_check_box_with_desc);
        check_box_layout->addWidget(background_check_box_);
        check_box_layout->addWidget(new QLabel(tr("<i>(for png and svg only)</i>")));

        auto form_layout = new QFormLayout(group);
        form_layout->setSpacing(12);
        form_layout->addRow(tr("&Cell size"), cell_size_input_);
        form_layout->addRow(tr("&Offset"), offset_input_);
        form_layout->addRow(tr("&Enable background"), bg_check_box_with_desc);

        auto button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);

        auto layout = new QVBoxLayout(this);
        layout->setSpacing(12);
        layout->addWidget(group);
        layout->addWidget(button_box);

        connect(button_box, &QDialogButtonBox::accepted, this, &Settings_dialog::do_accept);
        connect(button_box, &QDialogButtonBox::rejected, this, &Settings_dialog::reject);

    }

    void Settings_dialog::do_accept() {
        auto& export_settings = context_.settings().export_settings();
        export_settings.set_cell_size(cell_size_input_->value());
        export_settings.set_offset(offset_input_->value());
        export_settings.set_bg_enabled(background_check_box_->checkState() == Qt::Checked ? true : false); // TODO rename to bg_enabled()
        QDialog::accept();
    }

}
