#include "mouse_state.h"

#include <QPainter>

#include "commands.h"
#include "map_widget.h"
#include "math.h"

namespace Ttrpg_map_painter {

    Mouse_state::Mouse_state(Map_widget *painter) :map_widget_(painter) {}

    QPoint Mouse_state::calculate_nearest_point(QPoint point) {
        return calculate_nearest(point, map_widget_->cell_size_, -map_widget_->offset_);
    }


    Default_mouse_state::Default_mouse_state(Map_widget *painter) :Mouse_state(painter) {}

    void Default_mouse_state::on_paint(QPainter& painter) {
        if (map_widget_->paint_tool_ == Paint_tool::Wall) {
            auto pen = QPen("#333333"); // TODO pointer look and feel to painter class
            pen.setWidth(9);
            painter.setPen(pen);
        } else if (map_widget_->paint_tool_ == Paint_tool::Eraser) {
            auto pen = QPen("#FE8176");
            pen.setWidth(9);
            painter.setPen(pen);
        }
        painter.drawLine(map_widget_->current_point_, map_widget_->current_point_);
    }


    void Default_mouse_state::button_released(QMouseEvent *event) {
        if (event->button() == Qt::LeftButton) {
            map_widget_->previous_point_ = map_widget_->map_point(map_widget_->current_point_);
            map_widget_->mouse_state_ = &map_widget_->left_pressed_mouse_state_;
        }
    }


    Left_pressed_mouse_state::Left_pressed_mouse_state(Map_widget *painter) :Mouse_state(painter) {}

    void Left_pressed_mouse_state::on_paint(QPainter& painter) {
        QLine line(map_widget_->_point(map_widget_->previous_point_), map_widget_->_point(map_widget_->current_point_));
        if (map_widget_->paint_tool_ == Paint_tool::Eraser) {
            auto pen = QPen("#FE8176");
            pen.setWidth(6);
            painter.setPen(pen);
            painter.drawLine(line);
        } else
            map_widget_->map_painter_->wall_painter()->paint(painter, line);
    }

    class Add_line_command : public Command {
        private:
            Map& map_;
            QLine line_;
            std::vector<QLine> affected_;
        public:
            Add_line_command(Map& map, QLine line) :map_(map), line_(line) {}
            void execute() override { 
                map_.add(line_);
                map_.update_limits(line_);
                affected_ = map_.affected_lines();
            }
            void un_execute() override { 
                map_.remove(line_);
                for (auto& l : affected_)
                    map_.add(l);
                map_.update_limits();
            }
    };

    class Remove_line_command : public Command {
        private:
            Map& map_;
            QLine line_;
            std::vector<QLine> affected_;
        public:
            Remove_line_command(Map& map, QLine line) :map_(map), line_(line) {}
            void execute() override {
                map_.remove(line_);
                map_.update_limits();
                affected_ = map_.affected_lines();
            }
            void un_execute() override { 
                for (auto& l : affected_) {
                    map_.add(l);
                    map_.update_limits(l);
                }
            }
    };

    void Left_pressed_mouse_state::button_released(QMouseEvent *event) {
        if (event->button() == Qt::LeftButton) {
            QLine line(map_widget_->previous_point_, map_widget_->current_point_);
            if (map_widget_->paint_tool_ == Paint_tool::Wall)
                map_widget_->commands_executor_.execute<Add_line_command>(map_widget_->map_, line);
            else if (map_widget_->paint_tool_ == Paint_tool::Eraser)
                map_widget_->commands_executor_.execute<Remove_line_command>(map_widget_->map_, line);

            map_widget_->previous_point_ = map_widget_->current_point_;
        } else if (event->button() == Qt::RightButton)
            map_widget_->mouse_state_ = &map_widget_->default_mouse_state_;
    }

    QPoint Left_pressed_mouse_state::calculate_nearest_point(QPoint point) {
        point = map_widget_->map_point(point);
        if (std::abs(point.x() - map_widget_->previous_point_.x()) < std::abs(point.y() - map_widget_->previous_point_.y()))
            return {map_widget_->previous_point_.x(), point.y()};
        return {point.x(), map_widget_->previous_point_.y()};
    }

}
