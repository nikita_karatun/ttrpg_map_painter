#pragma once

#include <cstddef>
#include <utility>
#include <memory>
#include <vector>

namespace Ttrpg_map_painter {

    struct Commands_executor;

    class Command {
        private:
            friend class Commands_executor;
            Command *next_;
            Command *prev_;
        public:
            Command() :next_(nullptr), prev_(nullptr) {}

            virtual ~Command() {}
            virtual void execute() = 0;
            virtual void un_execute() = 0;
    };

    class Commands_executor {
        public:
            class Listener {
                public:
                    virtual ~Listener() {}
                    virtual void command_executed() {}
                    virtual void command_undone() {}
                    virtual void command_redone() {}
            };
        private:
            struct Empty_command : public Command {
                void execute() override {}
                void un_execute() override {}
            };
            struct Command_unique_ptr {
                Command *command_;
                Commands_executor *executor_;
                Command_unique_ptr(Command *, Commands_executor *);
                ~Command_unique_ptr();
                Command* operator->() { return command_; }
            };
            friend class Command_unique_ptr;

            Empty_command tail_;
            Command *head_;
            Command *persisted_;

            size_t size_;
            size_t count_;

            std::vector<std::unique_ptr<Listener>> listeners_;

        public:
            Commands_executor(size_t size);
            ~Commands_executor();

            void execute(Command *command);

            template<typename C, typename... Args>
                void execute(Args&&... args) {
                    execute(new C(std::forward<Args>(args)...));
                }

            bool has_undo_items();
            bool has_redo_items();

            void undo();
            void redo();

            void set_persisted();
            bool persisted();

            template<typename L, typename... Args>
                void emplace_listener(Args&&... args) {
                    listeners_.push_back(std::make_unique<L>(std::forward<Args>(args)...));
                }
    };

}
