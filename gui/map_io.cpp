#include "map_io.h"

#include <QJsonArray>
#include <QJsonObject>

#include <QPainter>

#include <QSvgGenerator>

#include "context.h"
#include "painting.h"

namespace Ttrpg_map_painter {

    namespace Map_io {

        QJsonDocument to_json(const Map& map) {
            QJsonArray lines;
            map.for_each_line([&lines](int x1, int y1, int x2, int y2) {
                QJsonObject line;
                line["x1"] = (x1);
                line["y1"] = (y1);
                line["x2"] = (x2);
                line["y2"] = (y2);
                lines.append(line);
            });

            QJsonObject root;
            root["walls"] = lines;
            QJsonDocument doc;
            doc.setObject(root);
            return doc;
        }

        Map from_json(const QJsonDocument& doc) {
            auto j_root = doc.object();
            auto j_lines = j_root["walls"].toArray();

            Map map;
            for (auto _j_line : j_lines) {
                auto j_line = _j_line.toObject();
                map.add(QLine(
                        j_line["x1"].toInt(),
                        j_line["y1"].toInt(),
                        j_line["x2"].toInt(),
                        j_line["y2"].toInt()));
            }
            return map;
        }

        struct Image_mcc : public Map_coordinates_converter {
            const Export_settings& export_settings_;
            const Map& map_;
            Image_mcc(const Settings& settings, const Map& map) :export_settings_(settings.export_settings()), map_(map) {}
            int _x(int x) override {
                return export_settings_.offset() + (x - map_.limits().min_x()) * export_settings_.cell_size();
            }
            int _y(int y) override {
                return export_settings_.offset() + (y - map_.limits().min_y()) * export_settings_.cell_size();
            }
        };

        struct Image_bg_painter : public Background_painter {
            void paint(QPainter&) override {}
        };

        struct Image_grid_painter : public Grid_painter {
            const Settings& settings_;
            const Map& map_;
            Image_grid_painter(const Settings& settings, const Map& map) 
                :settings_(settings), map_(map) 
            {}
            void paint(QPainter& painter) override {
                painter.setPen(settings_.map_style().grid_pen_);
                auto cell_size = settings_.export_settings().cell_size();
                auto offset = settings_.export_settings().offset();
                auto& limits = map_.limits();
                for (int i = 0; i <= limits.width(); ++i) {
                    auto x = offset + i * cell_size;
                    painter.drawLine(x, offset, x, painter.device()->height() - offset);
                }
                for (int i = 0; i <= limits.height(); ++i) {
                    auto y = offset + i * cell_size;
                    painter.drawLine(offset, y, painter.device()->width() - offset, y);
                }
            }
        };

        std::unique_ptr<Standard_map_painter> create_image_painter(const Settings& settings, const Map& map) {
            auto mcc = std::make_unique<Image_mcc>(settings, map);
            auto bg_painter = std::make_unique<Image_bg_painter>();
            auto grid_painter = std::make_unique<Image_grid_painter>(settings, map);
            auto wall_painter = std::make_unique<Standard_wall_painter>(settings);
            return std::make_unique<Standard_map_painter>(
                settings, map, std::move(mcc), std::move(bg_painter), std::move(grid_painter), std::move(wall_painter));
        }

        QSize calculate_image_size(Standard_map_painter *map_painter) {
            auto& limits = map_painter->map().limits();
            auto settings = map_painter->settings().export_settings();
            return {settings.offset() * 2 + settings.cell_size() * limits.width(),
                settings.offset() * 2 + settings.cell_size() * limits.height()};
        }

        void write(Standard_map_painter *map_painter, QPainter& painter) {
            painter.setRenderHint(QPainter::Antialiasing, true);
            map_painter->paint_bg(painter);
            map_painter->paint_grid(painter);
            map_painter->paint_walls(painter);
            map_painter->wall_painter()->post_process(painter);
        }

        QImage create_image(Standard_map_painter *map_painter, bool no_bg_supported) {
            auto no_bg = no_bg_supported && !map_painter->settings().export_settings().bg_enabled();
            QImage image;
            if (no_bg) {
                image = {calculate_image_size(map_painter), QImage::Format_ARGB32};
                image.fill(qRgba(0, 0, 0, 0));
            } else
                image = {calculate_image_size(map_painter), QImage::Format_RGB32};

            QPainter painter(&image);
            if (!no_bg)
                painter.fillRect(0, 0, image.width(), image.height(), QColor("white"));

            write(map_painter, painter);
            return image;
        }

        void write(Standard_map_painter *map_painter, QSvgGenerator& svg) {
            auto size = calculate_image_size(map_painter);
            svg.setSize(size);
            svg.setViewBox(QRect(0, 0, size.width(), size.height()));

            QPainter painter(&svg);
            if (map_painter->settings().export_settings().bg_enabled())
                painter.fillRect(0, 0, svg.width(), svg.height(), QColor("white"));
            write(map_painter, painter);
        }

        void Supported_file_types::Group::print(std::ostream& stream) const {
            stream << name_ << " (";
            auto ext = extenstions_.cbegin();

            if (ext != extenstions_.cend())
                stream << "*." << *ext;

            for (++ext; ext != extenstions_.cend(); ++ext)
                stream << " *." << *ext;
            stream << ")";
        }

        QString Supported_file_types::to_file_dialog_filter_str(const std::vector<Group>& groups) {
            std::stringstream ss;
            auto group = groups.cbegin();
            if (group != groups.cend())
                group->print(ss);

            for (++group; group != groups.cend(); ++group) {
                ss << ";;";
                group->print(ss);
            }
            return QString(ss.str().c_str());
        }

        std::set<QString> Supported_file_types::to_extensions_set(const std::vector<Group>& groups) {
            std::set<QString> set;
            for (auto group = groups.cbegin(); group != groups.cend(); ++group)
                for (auto ext  = group->cbegin(); ext != group->cend(); ++ext)
                    set.insert(QString(ext->c_str()));
            return set;
        }
    }

}
