#pragma once

#include <vector>
#include <map>
#include <list>
#include <iostream>
#include <functional>

#include <QLine>

namespace Ttrpg_map_painter {

    class Map_coordinates_converter {
        public:
            virtual ~Map_coordinates_converter() {}
            virtual int _x(int x) = 0;
            virtual int _y(int y) = 0;
    };

    class Interval {
        private:
            int begin_;
            int end_;
        public:
            static const Interval EMPTY;

            Interval(int begin, int end);
            bool try_merge(const Interval&);
            std::pair<Interval, Interval> erase_or_split(const Interval&);
            bool operator==(const Interval&) const;
            bool operator!=(const Interval& o) const { return !operator==(o); }

            int begin() const { return begin_; }
            int end() const { return end_; }

            bool empty() const { return begin_ == end_; }
    };

    std::ostream& operator<<(std::ostream&, const Interval&);

    class Map;

    class Intervals {
        private:
            std::list<Interval> intervals_;
            friend std::ostream& operator<<(std::ostream&, const Intervals&);
        public:
            Intervals() = default;
            Intervals(std::initializer_list<Interval>);

            void add_interval(int first, int second, Map *);
            void remove_interval(int first, int second, Map *);
            bool operator==(const Intervals& l) const { return intervals_ == l.intervals_; }
            auto begin() const { return intervals_.begin(); }
            auto end() const { return intervals_.end(); }
    };

    class Lines {
        private:
            std::map<int, Intervals> intervals_map_;
        public:
            void add_interval(int key, int first, int second, Map *);
            void remove_interval(int key, int first, int second, Map *);
            auto begin() const { return intervals_map_.begin(); }
            auto end() const { return intervals_map_.end(); }
            bool operator==(const Lines& o) const { return intervals_map_ == o.intervals_map_; }
            bool empty() { return intervals_map_.empty(); }
    };

    std::ostream& operator<<(std::ostream&, const Lines&);

    class Limits {
        private:
            int min_x_;
            int max_x_;
            int min_y_;
            int max_y_;
        public:
            Limits() :min_x_(0), max_x_(0), min_y_(0), max_y_(0) {}
            Limits(int min_x, int max_x, int min_y, int max_y) 
                :min_x_(min_x), max_x_(max_x), min_y_(min_y), max_y_(max_y) 
            {}
            bool empty() const { return min_x_ == 0 && max_x_ == 0 && min_y_ == 0 && max_y_ == 0; }
            void add(const QLine& line);
            bool operator==(const Limits& o) const {
                return min_x_ == o.min_x_
                    && max_x_ == o.max_x_
                    && min_y_ == o.min_y_
                    && max_y_ == o.max_y_;
            }
            int min_x() const { return min_x_; }
            int max_x() const { return max_x_; }
            int min_y() const { return min_y_; }
            int max_y() const { return max_y_; }

            int width() const { return max_x_ - min_x_; }
            int height() const { return max_y_ - min_y_; }
    };

    class Map {
        private:
            friend class Intervals;
            Lines h_lines_;
            Lines v_lines_;
            Limits limits_;
            std::vector<Interval> affected_intervals_;
            std::vector<QLine> affected_lines_;

            template<typename F>
                void modify(const QLine& line, F f);

            friend std::ostream& operator<<(std::ostream&, const Map&);
        public:
            Map();

            void add(const QLine&);
            void remove(const QLine&);

            void for_each_line(std::function<void(int, int, int, int)> f) const;
            void for_each_line(Map_coordinates_converter *, std::function<void(const QLine&)>) const;

            bool operator==(const Map&) const;
            bool operator!=(const Map& o) { return !operator==(o); }

            bool empty();

            void update_limits(const QLine& line);
            void update_limits();

            const Limits& limits() const { return limits_; }
            std::vector<QLine>& affected_lines() { return affected_lines_; }
    };

    std::vector<QLine> to_lines(const Map&);

}
