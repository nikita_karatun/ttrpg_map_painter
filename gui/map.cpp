#include "map.h"

#include <iostream>

#include "math.h"

namespace Ttrpg_map_painter {

    const Interval Interval::EMPTY = {0, 0};

    Interval::Interval(int begin, int end) : begin_(begin), end_(end) {
        if (begin_ > end_)
            std::swap(begin_, end_);
    }

    bool Interval::try_merge(const Interval& o) {
        bool merged = false;
        if (begin_ == o.begin_) {
            end_ = std::max(end_, o.end_);
            merged = true;
        } else if (begin_ >= o.begin_ && end_ <= o.end_) {
            begin_ = o.begin_;
            end_ = o.end_;
            merged = true;
        } else {
            Interval first = *this;
            Interval second = o;
            if (first.begin_ > second.begin_)
                std::swap(first, second);
            if (first.end_ > second.begin_) {
                if (first.end_ < second.end_) {
                    first.end_ = second.end_;
                    *this = first;
                }
                merged = true;
            }
        }
        return merged;
    }

    std::pair<Interval, Interval> Interval::erase_or_split(const Interval& e) {
        if (begin_ < e.begin_) {
            if (e.end_ < end_)
                return {{begin_, e.begin_}, {e.end_, end_}};
            else if (e.begin_ <= end_)
                return {{begin_, e.begin_}, EMPTY};
        }
        if (begin_ <= e.end_ && e.end_ < end_)
            return {{e.end_, end_}, EMPTY};
        if (begin_ >= e.begin_ && end_ <= e.end_)
            return {EMPTY, EMPTY};
        return {*this, EMPTY};
    }

    bool Interval::operator==(const Interval& o) const {
        return begin_ == o.begin_ && end_ == o.end_;
    }

    std::ostream& operator<<(std::ostream& stream, const Interval& i) {
        return stream << "[" << i.begin() << ":" << i.end() << "]";
    }

    Intervals::Intervals(std::initializer_list<Interval> i_list) :intervals_(i_list) {}

    void Intervals::add_interval(int first, int second, Map *map) {
        Interval new_i(first, second);
        if (intervals_.empty()) {
            intervals_.emplace_back(first, second);
        } else {
            bool inserted = false;
            bool merged = false;
            for (auto i = intervals_.begin(); !inserted && i != intervals_.end(); ++i) {
                auto old = *i;
                if (i->try_merge(new_i)) {
                    new_i = *i;
                    merged = true;
                    map->affected_intervals_.push_back(old);
                }
                if (!merged && new_i.end() < i->begin()) {
                    intervals_.insert(i, new_i);
                    inserted = true;
                }
            }
            if (!inserted && !merged)
                intervals_.push_back(new_i);
        }
    }

    void Intervals::remove_interval(int first, int second, Map *map) {
        const Interval e(first, second);
        for (auto i = intervals_.begin(); i != intervals_.end(); ) {
            auto current = i++;
            auto pair = current->erase_or_split(e);
            if (pair.first != *current) {
                map->affected_intervals_.push_back(*current);
                if (pair.second == Interval::EMPTY) {
                    if (pair.first == Interval::EMPTY)
                        intervals_.erase(current);
                    else 
                        *current = pair.first;
                } else {
                    *current = pair.first;
                    intervals_.insert(i, pair.second);
                    i = intervals_.end();
                }
            }
        }
    }

    std::ostream& operator<<(std::ostream& stream, const Intervals& l) {
        auto p = l.intervals_.cbegin();
        if (p != l.intervals_.cend())
            stream << *p;
        for (++p; p != l.intervals_.cend(); ++p)
            stream << ", " << *p;
        return stream;
    }

    void Lines::add_interval(int key, int first, int second, Map *map) {
        auto line = intervals_map_.find(key);
        if (line == intervals_map_.end()) {
            auto& intervals = intervals_map_[key];
            intervals = {};
            intervals.add_interval(first, second, map);
        } else
            line->second.add_interval(first, second, map);
    }

    void Lines::remove_interval(int key, int first, int second, Map *map) {
        auto line = intervals_map_.find(key);
        if (line != intervals_map_.end())
            line->second.remove_interval(first, second, map);
    }

    Map::Map() {
        affected_intervals_.reserve(10);
        affected_lines_.reserve(10);
    }

    template<typename F>
        void Map::modify(const QLine& line, F f) {
            affected_intervals_.clear();
            affected_lines_.clear();

            auto p1 = line.p1();
            auto p2 = line.p2();

            int key;
            int begin;
            int end;
            Lines *lines = nullptr;

            if (p1.x() == p2.x()) {
                key = p1.x();
                begin = p1.y();
                end = p2.y();
                lines = &v_lines_;
            } else if (p1.y() == p2.y()) {
                key = p1.y();
                begin = p1.x();
                end = p2.x();
                lines = &h_lines_;
            }

            if (!lines)
                return;

            f(lines, key, begin, end);

            for (auto& i : affected_intervals_)
                if (lines == &h_lines_)
                    affected_lines_.emplace_back(i.begin(), key, i.end(), key);
                else
                    affected_lines_.emplace_back(key, i.begin(), key, i.end());
        }

    void Map::add(const QLine& line) {
        modify(line, [this, &line](Lines *lines, int key, int begin, int end) {
            lines->add_interval(key, begin, end, this);
        });
    }

    void Map::remove(const QLine& line) {
        modify(line, [this, &line](Lines *lines, int key, int begin, int end) {
            lines->remove_interval(key, begin, end, this);
        });
    }

    void Map::for_each_line(std::function<void(int, int, int, int)> f) const { // use QLine?
        for (auto e = v_lines_.begin(); e != v_lines_.end(); ++e) {
            auto x = e->first;
            for (auto p : e->second)
                f(x, p.begin(), x, p.end());
        }
        for (auto e = h_lines_.begin(); e != h_lines_.end(); ++e) {
            auto y = e->first;
            for (auto p : e->second)
                f(p.begin(), y, p.end(), y);
        }
    }

    void Map::for_each_line(Map_coordinates_converter * mcc, std::function<void(const QLine&)> f) const {
        for (auto e = v_lines_.begin(); e != v_lines_.end(); ++e) {
            auto x = mcc->_x(e->first);
            for (auto p : e->second)
                f({x, mcc->_y(p.begin()), x, mcc->_y(p.end())});
        }
        for (auto e = h_lines_.begin(); e != h_lines_.end(); ++e) {
            auto y = mcc->_y(e->first);
            for (auto p : e->second)
                f({mcc->_x(p.begin()), y, mcc->_x(p.end()), y});
        }
    } 

    std::ostream& operator<<(std::ostream& stream, const Lines& ls) {
        for (auto e = ls.begin(); e != ls.end(); ++e)
            stream << e->first << ": " << e->second << std::endl;
        return stream;
    }

    void Limits::add(const QLine& l) {
        if (empty()) {
            min_x_ = calculate_min(l.p1().x(), l.p2().x());
            max_x_ = calculate_max(l.p1().x(), l.p2().x());

            min_y_ = calculate_min(l.p1().y(), l.p2().y());
            max_y_ = calculate_max(l.p1().y(), l.p2().y());
        } else {
            min_x_ = calculate_min(min_x_, l.p1().x(), l.p2().x());
            max_x_ = calculate_max(max_x_, l.p1().x(), l.p2().x());

            min_y_ = calculate_min(min_y_, l.p1().y(), l.p2().y());
            max_y_ = calculate_max(max_y_, l.p1().y(), l.p2().y());
        }
    }

    bool Map::operator==(const Map& o) const {
        return v_lines_ == o.v_lines_ && h_lines_ == o.h_lines_;
    }

    bool Map::empty() {
        return v_lines_.empty() && h_lines_.empty();
    }

    void Map::update_limits(const QLine& l) {
        limits_.add(l);
    }

    void Map::update_limits() {
        limits_ = {};
        for_each_line([this](int x1, int y1, int x2, int y2) {
            update_limits({x1, y1, x2, y2});
        });
    }

    std::vector<QLine> to_lines(const Map& map) {
        std::vector<QLine> lines;
        map.for_each_line([&lines](int x1, int y1, int x2, int y2) {
            lines.emplace_back(x1, y1, x2, y2);
        });
        return lines;
    }

    std::ostream& operator<<(std::ostream& stream, const Map& m) {
        return stream << "vertical lines (x const):" << std::endl << m.v_lines_ <<
            "horizontal lines (y const):" << std::endl << m.h_lines_;
    }

}
