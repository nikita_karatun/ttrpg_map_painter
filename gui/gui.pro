#-------------------------------------------------
#
# Project created by QtCreator 2020-05-26T20:56:09
#
#-------------------------------------------------

QT       += core gui svg
QMAKE_CXXFLAGS += "-fno-sized-deallocation"

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    mouse_state.cpp \
    map.cpp \
    map_io.cpp \
    commands.cpp \
    arrow.cpp \
    map_widget.cpp \
    painting.cpp \
    context.cpp \
    settings_dialog.cpp \
    math.cpp

HEADERS += \
        mainwindow.h \
    mouse_state.h \
    map.h \
    map_io.h \
    commands.h \
    arrow.h \
    map_widget.h \
    painting.h \
    context.h \
    settings_dialog.h \
    math.h
