#pragma once

#include <QDialog>

class QSpinBox;
class QCheckBox;

namespace Ttrpg_map_painter {

    class Application_context;

    class Settings_dialog : public QDialog {
        private:
            Application_context& context_;

            QSpinBox *cell_size_input_;
            QSpinBox *offset_input_;
            QCheckBox *background_check_box_;
        public:
            Settings_dialog(QWidget *parent, Application_context& context);

            void do_accept();
    };
}
