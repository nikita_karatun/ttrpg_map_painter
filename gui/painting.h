#pragma once

#include <functional>
#include <memory>

#include <QPen>

#include "map.h"

class QPainter;

namespace Ttrpg_map_painter {

    class Map;
    class Settings;

    void multi_pen_draw(QPainter& painter, const std::vector<QPen>& pens, std::function<void()> func);

    QPen pen(QColor color, int width);

    struct Map_style {
        QColor main_bg_;
        QColor map_bg_;
        QPen grid_pen_;
        std::vector<QPen> wall_pen_;
        QPen arrow_pen_;
    };

    struct Wall_painter {
        virtual ~Wall_painter() {}
        virtual void paint(QPainter& painter, QLine wall) = 0;
        virtual void post_process(QPainter&) {}
    };

    struct Background_painter {
        virtual ~Background_painter() {}
        virtual void paint(QPainter& painter) = 0;
    };

    struct Grid_painter {
        virtual ~Grid_painter() {}
        virtual void paint(QPainter& painter) = 0;
    };

    class Standard_map_painter { // TODO rename?
        private:
            const Settings& settings_;
            const Map& map_;
            std::unique_ptr<Map_coordinates_converter> mcc_;
            std::unique_ptr<Background_painter> bg_painter_;
            std::unique_ptr<Grid_painter> grid_painter_;
            std::unique_ptr<Wall_painter> wall_painter_;
        public:
            Standard_map_painter(const Settings& settings,
                                 const Map& map, 
                                 std::unique_ptr<Map_coordinates_converter> mcc, 
                                 std::unique_ptr<Background_painter> bg_painter, 
                                 std::unique_ptr<Grid_painter> grid_painter, 
                                 std::unique_ptr<Wall_painter> wall_painter)
                :settings_(settings),
                map_(map),
                mcc_(std::move(mcc)), 
                bg_painter_(std::move(bg_painter)),
                grid_painter_(std::move(grid_painter)),
                wall_painter_(std::move(wall_painter)) {}

            void paint_bg(QPainter& painter) { bg_painter_->paint(painter); }

            void paint_grid(QPainter& painter) { grid_painter_->paint(painter); }

            void paint_walls(QPainter& painter);

            Wall_painter* wall_painter() { return wall_painter_.get(); }

            const Settings& settings() { return settings_; }
            const Map& map() { return map_; }
    };

    class Standard_wall_painter : public Wall_painter {
        private:
            std::vector<QLine> walls_;
            const Map_style& style_;
        public:
            Standard_wall_painter(const Settings& settings);
            void paint(QPainter& painter, QLine wall) override;
            void post_process(QPainter& painter) override;
    };

}
