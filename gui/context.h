#pragma once

#include "map.h"
#include "map_io.h"
#include "commands.h"
#include "painting.h"

namespace Ttrpg_map_painter {

    class UI_settings {
        private:
            bool tools_enabled_;
        public:
            bool tools_enabled() { return tools_enabled_; }
            void set_tools_enabled(bool enabled) { tools_enabled_ = enabled; }
    };

    class Export_settings {
        private:
            int offset_ = 40;
            int cell_size_ = 40;
            bool enable_bg_ = false;
        public:
            int offset() const { return offset_; }
            int cell_size() const { return cell_size_; }
            bool bg_enabled() const { return enable_bg_; }

            void set_offset(int offset) { offset_ = offset; }
            void set_cell_size(int cell_size) { cell_size_ = cell_size; }
            void set_bg_enabled(bool enable) { enable_bg_ = enable; }
    };

    class Settings {
        private:
            UI_settings ui_settings_;
            Export_settings export_settings_;
            Map_style map_style_;
        public:
            Settings() {
                map_style_.main_bg_ = {"#CCCCCC"};
                map_style_.map_bg_ = {"#EEEEEE"};
                map_style_.grid_pen_ = {"#666666"};
                map_style_.wall_pen_ = {pen(QColor("#333333"), 6), pen(QColor("#EEEEEE"), 2)};
                map_style_.arrow_pen_ = pen(QColor("#1B3409"), 3);
            }

            const UI_settings& ui_settings() const { return ui_settings_; }
            UI_settings& ui_settings() { return ui_settings_; }

            const Export_settings& export_settings() const { return export_settings_; }
            Export_settings& export_settings() { return export_settings_; }

            const Map_style& map_style() const { return map_style_; }
    };

    class Application_context {
        private:
            Commands_executor commands_executor_;
            Map map_;
            Settings settings_;
            std::unique_ptr<Standard_map_painter> map_painter_;
            const Map_io::Supported_file_types map_file_type_;
            const Map_io::Supported_file_types supported_export_types_;
        public:
            Application_context();
            ~Application_context();

            Commands_executor& commands_executor() { return commands_executor_; }
            Map& map() { return map_; }

            Settings& settings() { return settings_; }

            Standard_map_painter* map_painter() { return map_painter_.get(); }
            const Map_io::Supported_file_types& map_file_type() { return map_file_type_; }
            const Map_io::Supported_file_types& supported_export_types() { return supported_export_types_; }
    };

}
