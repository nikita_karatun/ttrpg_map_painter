#include "mainwindow.h"

#include <QApplication>

#include "context.h"

int main(int argc, char** argv) {
    QApplication a(argc, argv);

    Ttrpg_map_painter::Application_context context;

    Ttrpg_map_painter::Main_window w(context);
    w.show();

    return a.exec();
}
