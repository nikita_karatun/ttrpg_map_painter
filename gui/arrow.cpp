#include "arrow.h"

#include <cmath>

#include <QLine>
#include <QPainter>

#include "map_widget.h"

namespace Ttrpg_map_painter {

    Arrow_position::Arrow_position(Map_widget *map_widget) :map_widget_(map_widget) {

        struct Hor_calculator : public Arrow_position::Calculator {
            Hor_calculator(Map_widget *painter) :Calculator(Linear_equation::Type::Horizontal, painter) {}
            virtual int calculate_y() = 0;
            bool do_calculate(const Linear_equation& lin_eq) override {
                auto y = calculate_y();
                auto x = lin_eq.x(y);
                if (55 <= x && x <= map_widget_->width() - 55) {
                    arrow_center_ = {x, y};
                    return true;
                }
                return false;
            }
        };

        struct Top_calculator : public Hor_calculator {
            Top_calculator(Map_widget *painter) :Hor_calculator(painter) {}
            bool outsize_view(const Limits& limits) override {
                return 0 > map_widget_->_y(limits.max_y() + 2);
            }
            int calculate_y() override { return 55; }
        };

        struct Bottom_calculator : public Hor_calculator {
            Bottom_calculator(Map_widget *painter) :Hor_calculator(painter) {}
            bool outsize_view(const Limits& limits) override {
                return map_widget_->height() < map_widget_->_y(limits.min_y() - 2);
            }
            int calculate_y() override { return map_widget_->height() - 55; }
        };

        struct Ver_calculator : public Arrow_position::Calculator {
            Ver_calculator(Map_widget *painter) :Calculator(Linear_equation::Type::Horizontal, painter) {}
            virtual int calculate_x() = 0;
            bool do_calculate(const Linear_equation& lin_eq) override {
                auto x = calculate_x();
                auto y = lin_eq.y(x);
                if (55 <= y && y <= map_widget_->width() - 55) {
                    arrow_center_ = {x, y};
                    return true;
                }
                return false;
            }
        };

        struct Left_calculator : public Ver_calculator {
            Left_calculator(Map_widget *painter) :Ver_calculator(painter) {}
            bool outsize_view(const Limits& limits) override {
                return 0 > map_widget_->_x(limits.max_x() + 2);
            }
            int calculate_x() override { return 55; }
        };

        struct Right_calculator : public Ver_calculator {
            Right_calculator(Map_widget *painter) :Ver_calculator(painter) {}
            bool outsize_view(const Limits& limits) override {
                return map_widget_->width() < map_widget_->_x(limits.min_x() - 2);
            }
            int calculate_x() override { return map_widget_->width() - 55; }
        };

        calculators_.push_back(std::make_unique<Top_calculator>(map_widget_));
        calculators_.push_back(std::make_unique<Bottom_calculator>(map_widget_));
        calculators_.push_back(std::make_unique<Left_calculator>(map_widget_));
        calculators_.push_back(std::make_unique<Right_calculator>(map_widget_));

    }

    void Arrow_position::update() {
        arrow_.set_null();
        auto limits = map_widget_->map_.limits();
        if (limits.empty())
            return;

        auto x = limits.min_x() - 2;
        auto y = limits.min_y() - 2;
        auto w = limits.max_x() - x + 2;
        auto h = limits.max_y() - y + 2;

        QPoint canvas_center(map_widget_->_x(x + w / 2), map_widget_->_y(y + h / 2)); // TODO fix center calculation
        QPoint display_center(map_widget_->width() / 2, map_widget_->height() / 2);

        QLine centers_line = {display_center, canvas_center};

        Linear_equation linear_eq(centers_line);

        QPoint arrow_center;
        for (auto& calculator : calculators_) {
            if (calculator->calculate(limits, linear_eq)) {
                arrow_center = calculator->arrow_center_;
                break;
            }
        }
        if (!arrow_center.isNull()) {
            auto diff = canvas_center - display_center;
            auto arrow_angle = std::atan2(diff.y(), diff.x());
            arrow_.set_position(arrow_center, arrow_angle);
        }
    }

    Arrow::Arrow() :lines_(4), is_null_(true) {}

    void Arrow::set_position(QPoint center, float angle) {
        is_null_ = center.isNull();
        if (is_null_)
            return;

        auto it = lines_.begin();
        auto add_line = [this, &it, center, angle](const QLine& line) {
            *it++ = rotate(line, center, angle);
        };

        add_line({center.x() - 20, center.y() + 5, center.x(), center.y() + 5});
        add_line({center.x() - 20, center.y() - 5, center.x(), center.y() - 5});

        add_line({center.x(), center.y() + 10, center.x() + 20, center.y()});
        add_line({center.x(), center.y() - 10, center.x() + 20, center.y()});
    }

    void Arrow::draw(QPainter& painter) {
        for (auto& line : lines_)
            painter.drawLine(line);
    }

}
