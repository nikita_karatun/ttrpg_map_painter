#include "context.h"

#include <QSettings>

#include "map_io.h"

namespace Ttrpg_map_painter {

    template<typename T>
        class Settings_value_handler {
            private:
                QString key_;
                T default_;
            public:
                Settings_value_handler(const QString& key, T d) :key_(key), default_(d) {}

                T get(QSettings& settings) {
                    QVariant variant = settings.value(key_, default_);
                    return variant.value<T>();
                }
                void set(QSettings& settings, T value) {
                    settings.setValue(key_, value);
                }
        };

    class Settings_handler {
        private:
            QSettings settings_;
        public:
            Settings_handler() :settings_("ttrpg_map_painter", "ttrpg_map_painter") {}
            QSettings& settings() { return settings_; }
    };

    Settings_value_handler<bool> tools_dock_enabled_svh("ui/tools_dock_enabled", true);

    Settings_value_handler<int> cell_size_svh("export/cell_size", 40);
    Settings_value_handler<int> offset_svh("export/offset", 40);
    Settings_value_handler<bool> bg_enabled_svh("export/bg_enabled", true);

    Application_context::Application_context()
        :commands_executor_(100),  // TODO move count to settings?
        map_file_type_({{"Map file", {"json"}}}),
        supported_export_types_({{"Image Files", {"jpg", "png", "bmp"}}, {"SVG", {"svg"}}})
        {
            map_painter_ = Map_io::create_image_painter(settings_, map_);

            Settings_handler settings_handler;
            auto& qs = settings_handler.settings();

            settings_.ui_settings().set_tools_enabled(tools_dock_enabled_svh.get(qs));

            auto& export_settings = settings_.export_settings();
            export_settings.set_cell_size(cell_size_svh.get(qs));
            export_settings.set_offset(offset_svh.get(qs));
            export_settings.set_bg_enabled(bg_enabled_svh.get(qs));
        }

    Application_context::~Application_context() {
        Settings_handler settings_handler;
        auto& qs = settings_handler.settings();

        tools_dock_enabled_svh.set(qs, settings_.ui_settings().tools_enabled());

        auto& export_settings = settings_.export_settings();
        cell_size_svh.set(qs, export_settings.cell_size());
        offset_svh.set(qs, export_settings.offset());
        bg_enabled_svh.set(qs, export_settings.bg_enabled());
    }

}
