#include "math.h"

#include <cmath>

namespace Ttrpg_map_painter {

    int calculate_first_non_negative(int step, int offset) {
        int p;
        if (offset == 0)
            p = 0;
        else if (offset > 0)
            p = offset % step;
        else
            for (p = offset; p <= 0; p += step);
        return p;
    }

    int calculate_nearest(int coord, int step, int offset) {
        auto first = calculate_first_non_negative(step, offset);
        auto steps = (coord - first) / step;
        auto min = steps * step + first; 
        auto max = min + step;
        return coord - min < max - coord ? min : max;
    }

    QPoint calculate_nearest(QPoint point, int step, QPoint offset) {
        return {calculate_nearest(point.x(), step, offset.x()), calculate_nearest(point.y(), step, offset.y())};
    }

    int calculate_first_grid_value(int step, int offset) {
        if (offset == 0)
            return 0;
        double d = double(offset) / step;
        if (offset > 0)
            d += .5;
        return int(d);
    }

    QPoint rotate(QPoint point, QPoint pivot, float angle) {
        float s = std::sin(angle);
        float c = std::cos(angle);

        point -= pivot;

        float x_rotated = point.x() * c - point.y() * s;
        float y_rotated = point.x() * s + point.y() * c;

        point.setX(x_rotated + pivot.x());
        point.setY(y_rotated + pivot.y());
        return point;
    }

    QLine rotate(QLine line, QPoint pivot, float angle) {
        return {rotate(line.p1(), pivot, angle), rotate(line.p2(), pivot, angle)};
    }

    Linear_equation::Linear_equation(QLine line) :type_(Type::Inclined) {
        auto numerator = line.p1().y() - line.p2().y();
        if (numerator == 0) {
            type_ = Type::Horizontal;
            coord_ = line.p1().y();
        } else {
            auto denominator = line.p1().x() - line.p2().x();
            if (denominator == 0) {
                type_ = Type::Vertical;
                coord_ = line.p1().x();
            } else {
                k_ = float(numerator) / denominator;
                b_ = line.p1().y() - k_ * line.p1().x();
            }
        }
    }

    int Linear_equation::x(int y) const { // TODO y == 0
        return type_ == Type::Inclined ? int(float(y) / k_ - b_/k_) : coord_;
    }

    int Linear_equation::y(int x) const {
        return type_ == Type::Inclined ? int(k_ * x + b_) : coord_;
    }

}
