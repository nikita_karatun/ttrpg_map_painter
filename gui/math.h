#pragma once

#include <QPoint>
#include <QLine>

namespace Ttrpg_map_painter {

    int calculate_first_non_negative(int step, int offset);

    int calculate_nearest(int coord, int step, int offset);

    QPoint calculate_nearest(QPoint point, int step, QPoint offset);

    int calculate_first_grid_value(int step, int offset);

    int calculate_nearest_grid_value(int coord, int step, int offset);

    QPoint calculate_nearest_grid_point(QPoint point, int step, QPoint offset);

    QPoint rotate(QPoint point, QPoint pivot, float angle);

    QLine rotate(QLine line, QPoint pivot, float angle);

    template<typename T>
        struct Max_calculator {
            T value_;
            Max_calculator(T value) :value_(value) {}
            template<typename... Args>
                void calculate(T t, Args&&... args) {
                    value_ = std::max(value_, t);
                    calculate(args...);
                }
            void calculate(T t) {
                value_ = std::max(value_, t);
            }
        };

    template<typename T, typename... Args>
        T calculate_max(T t, Args... args) {
            Max_calculator<T> calculator(t);
            calculator.calculate(args...);
            return calculator.value_;
        }

    template<typename T>
        struct Min_calculator {
            T value_;
            Min_calculator(T value) :value_(value) {}
            template<typename... Args>
                void calculate(T t, Args&&... args) {
                    value_ = std::min(value_, t);
                    calculate(args...);
                }
            void calculate(T t) {
                value_ = std::min(value_, t);
            }
        };

    template<typename T, typename... Args>
        T calculate_min(T t, Args... args) {
            Min_calculator<T> calculator(t);
            calculator.calculate(args...);
            return calculator.value_;
        }

    class Linear_equation {
        public:
            enum class Type {
                Inclined, Vertical, Horizontal 
            };
        private:
            Type type_;
            float k_;
            float b_;
            int coord_;
        public:
            Linear_equation(QLine);
            int x(int y) const;
            int y(int x) const;
            Type type() const { return type_; }
    };

}
