#pragma once

#include <QWidget>

#include "map.h"
#include "mouse_state.h"
#include "arrow.h"

#include "painting.h"

namespace Ttrpg_map_painter {

    class Application_context;
    class Commands_executor;

    enum class Paint_tool {
        Wall, Eraser
    };

    class Map_widget : public QWidget, public Map_coordinates_converter {
        private:
            friend class Mouse_state;
            friend class Default_mouse_state;
            friend class Left_pressed_mouse_state;
            friend class Arrow_position;

            Map& map_;
            const Map_style& style_;
            std::unique_ptr<Standard_map_painter> map_painter_;
            Arrow_position arrow_position_;

            Commands_executor& commands_executor_;

            Paint_tool paint_tool_;
            int cell_size_;

            Default_mouse_state default_mouse_state_;
            Left_pressed_mouse_state left_pressed_mouse_state_;
            Mouse_state *mouse_state_;

            QPoint current_point_;
            QPoint previous_point_;

            QPoint offset_;
            QPoint mouse_middle_pos_;

        protected:
            void paintEvent(QPaintEvent *event);
            void mousePressEvent(QMouseEvent *event);
            void mouseReleaseEvent(QMouseEvent *event);
            void mouseMoveEvent(QMouseEvent *event);
            void leaveEvent(QEvent *event);
            void resizeEvent(QResizeEvent *event);

        public:
            explicit Map_widget(QWidget *parent, Application_context&);

            QSize minimumSizeHint() const override;
            QSize sizeHint() const override;

            // Map_coordinates_converter
            int _x (int x) override;
            int _y (int y) override;

            QPoint _point(QPoint);
            QLine _line(const QLine& line);

            QPoint map_point(QPoint);
            QLine map_line(const QLine&);

            void set_pain_tool(Paint_tool p) { paint_tool_ = p; }

            const Map& map() { return map_; }
            int cell_size() { return cell_size_; }
            QPoint offset() { return offset_; }
            const Map_style& map_style() { return style_; }
    };

}
