#include "painting.h"

#include <QPainter>

#include "context.h"
#include "map.h"

namespace Ttrpg_map_painter {

    void multi_pen_draw(QPainter& painter, const std::vector<QPen>& pens, std::function<void()> func) {
        for (auto pen = pens.cbegin(); pen != pens.cend(); ++pen) {
            painter.setPen(*pen);
            func();
        }
    };

    QPen pen(QColor color, int width) {
        QPen pen(color);
        pen.setWidth(width);
        return pen;
    }

    void Standard_map_painter::paint_walls(QPainter& painter) {
        map_.for_each_line(mcc_.get(), [this, &painter](const QLine& line) {
            wall_painter_->paint(painter, line);
        });
    }

    Standard_wall_painter::Standard_wall_painter(const Settings& settings) :style_(settings.map_style()) {
        walls_.reserve(50);
    }

    void Standard_wall_painter::paint(QPainter& painter, QLine wall) {
        painter.setPen(style_.wall_pen_[0]);
        painter.drawLine(wall);
        walls_.push_back(wall);
    }

    void Standard_wall_painter::post_process(QPainter& painter) {
        painter.setPen(style_.wall_pen_[1]);
        for (auto& wall : walls_)
            painter.drawLine(wall);
        walls_.clear();
    }

}
