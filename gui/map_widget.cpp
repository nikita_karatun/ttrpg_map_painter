#include "map_widget.h"

#include <iostream>
#include <cmath>
#include <memory>

#include <QPainter>
#include <QMouseEvent>

#include "context.h"
#include "math.h"
#include "arrow.h"

namespace Ttrpg_map_painter {

    struct Widget_mcc : public Map_coordinates_converter {
        Map_widget *map_widget_;
        Widget_mcc(Map_widget *map_widget) :map_widget_(map_widget) {}
        int _x(int x) override {
            return map_widget_->_x(x);
        }
        int _y(int y) override {
            return map_widget_->_y(y);
        }
    };

    struct Widget_background_painter : public Background_painter {
        Map_widget *map_widget_;
        Widget_background_painter(Map_widget *map_widget) 
            :map_widget_(map_widget) 
        {}
        void paint(QPainter& painter) override {
            painter.fillRect(0, 0, map_widget_->width(), map_widget_->height(), map_widget_->map_style().main_bg_);

            auto& limits = map_widget_->map().limits();
            if (!limits.empty()) {
                painter.fillRect(map_widget_->_x(limits.min_x() - 2), map_widget_->_y(limits.min_y() - 2),
                                 (limits.width() + 4) * map_widget_->cell_size(), 
                                 (limits.height() + 4) * map_widget_->cell_size(), map_widget_->map_style().map_bg_);
            }
        }
    };

    struct Widget_grid_painter : public Grid_painter {
        Map_widget* map_widget_;
        Widget_grid_painter(Map_widget* map_widget) :map_widget_(map_widget) {}
        void paint(QPainter& painter) override {
            painter.setPen(map_widget_->map_style().grid_pen_);
            auto cell_size = map_widget_->cell_size();
            auto offset = map_widget_->offset();
            for (int i = map_widget_->_x(calculate_first_grid_value(cell_size, offset.x()));
                 i < map_widget_->width(); i += cell_size)
                painter.drawLine(i, 0, i, map_widget_->height());
            for (int i = map_widget_->_y(calculate_first_grid_value(cell_size, offset.y()));
                 i < map_widget_->width(); i += cell_size)
                painter.drawLine(0, i, map_widget_->width(), i);
        }
    };

    Map_widget::Map_widget(QWidget *parent, Application_context& context) 
        : QWidget(parent),
        map_(context.map()),
        style_(context.settings().map_style()),
        arrow_position_(this),
        commands_executor_(context.commands_executor()),
        paint_tool_(Paint_tool::Wall),
        cell_size_(30),
        default_mouse_state_(this),
        left_pressed_mouse_state_(this),
        mouse_state_(&default_mouse_state_)
    {
        auto mcc = std::make_unique<Widget_mcc>(this);
        auto bg_painter = std::make_unique<Widget_background_painter>(this);
        auto grid_painter = std::make_unique<Widget_grid_painter>(this);
        auto wall_painter = std::make_unique<Standard_wall_painter>(context.settings());

        map_painter_ = std::make_unique<Standard_map_painter>(
            context.settings(), map_, std::move(mcc),
            std::move(bg_painter), std::move(grid_painter), std::move(wall_painter));

        setMouseTracking(true); // mouseMoveEvent triggered even if mouse button is not pressed
    }

    QSize Map_widget::minimumSizeHint() const { return {100, 100}; }

    QSize Map_widget::sizeHint() const { return {400, 200}; }

    int Map_widget::_x (int x) {
        return cell_size_ * x - offset_.x();
    }

    int Map_widget::_y (int y) {
        return cell_size_ * y - offset_.y();
    }

    QPoint Map_widget::_point(QPoint p) {
        return {_x(p.x()), _y(p.y())};
    }

    QLine Map_widget::_line(const QLine& line) {
        return {_point(line.p1()), _point(line.p2())};
    }

    QPoint Map_widget::map_point(QPoint point) {
        return (point + offset_) / cell_size_;
    }

    QLine Map_widget::map_line(const QLine& line) {
        return {map_point(line.p1()), map_point(line.p2())};
    }

    void Map_widget::paintEvent(QPaintEvent *) {
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing, true);

        map_painter_->paint_bg(painter);
        map_painter_->paint_grid(painter);
        map_painter_->paint_walls(painter);

        if (underMouse())
            mouse_state_->on_paint(painter);

        map_painter_->wall_painter()->post_process(painter);

        if (!arrow_position_.arrow_.is_null()) {
            painter.setPen(style_.arrow_pen_);
            arrow_position_.arrow_.draw(painter);
        }
    }

    void Map_widget::mousePressEvent(QMouseEvent * event) {
        if (event->button() == Qt::MidButton) {
            mouse_middle_pos_ = event->pos();
            setCursor(Qt::ClosedHandCursor);
        }
    }

    void Map_widget::mouseReleaseEvent(QMouseEvent *event) {
        if (event->button() == Qt::MidButton) {
            mouse_middle_pos_ = {};
            unsetCursor();
        }
        mouse_state_->button_released(event);
    }

    void Map_widget::mouseMoveEvent(QMouseEvent *event) {
        auto to_update = false;
        const auto pos = event->pos();
        if (event->buttons() == Qt::MidButton && !mouse_middle_pos_.isNull()) {
            offset_ = {offset_.x() + mouse_middle_pos_.x() - pos.x(), offset_.y() + mouse_middle_pos_.y() - pos.y()};
            mouse_middle_pos_ = pos;
            arrow_position_.update();
            to_update = true;
        }
        auto new_current_point = mouse_state_->calculate_nearest_point(pos);
        if (new_current_point != current_point_) {
            current_point_ = new_current_point;
            to_update = true;
        }
        if (to_update)
            update();
    }

    void Map_widget::leaveEvent(QEvent *) { update(); }

    void Map_widget::resizeEvent(QResizeEvent *) { arrow_position_.update(); }

}
