#include "commands.h"

#include <memory>

namespace Ttrpg_map_painter {

    Commands_executor::Command_unique_ptr::Command_unique_ptr(Command *command, Commands_executor *executor) 
        :command_(command), executor_(executor) 
    {}

    Commands_executor::Command_unique_ptr::~Command_unique_ptr() {
        if (executor_->persisted_ == command_)
            executor_->persisted_ = nullptr;
        delete command_;
    }

    Commands_executor::Commands_executor(size_t size) 
        :head_(&tail_), persisted_(&tail_), size_(size), count_(0) 
    {}

    Commands_executor::~Commands_executor() {
        for (Command *c = tail_.next_; c; ) {
            Command_unique_ptr to_delete(c, this);
            c = c->next_;
        }
    }

    void Commands_executor::execute(Command *command)  {
        for (auto c = head_->next_; c; ) {
            Command_unique_ptr to_delete(c, this);
            c = c->next_;
            --count_;
        }
        command->prev_ = head_;
        head_->next_ = command;
        head_ = command;

        ++count_;
        if (count_ > size_) {
            Command_unique_ptr to_delete(tail_.next_, this);
            tail_.next_ = to_delete->next_;
            tail_.next_->prev_ = &tail_;
        }
        command->execute();
        for (auto& l : listeners_)
            l->command_executed();
    }

    bool Commands_executor::has_undo_items() {
        return head_ != &tail_;
    }

    bool Commands_executor::has_redo_items() {
        return head_->next_;
    }

    void Commands_executor::undo() {
        head_->un_execute();
        if (head_->prev_)
            head_ = head_->prev_;
        for (auto& l : listeners_)
            l->command_undone();
    }

    void Commands_executor::redo() {
        if (head_->next_) { // TODO do the check?
            head_->next_->execute();
            head_ = head_->next_;
        }
        for (auto& l : listeners_)
            l->command_redone();
    }

    void Commands_executor::set_persisted() {
        persisted_ = head_;
    }

    bool Commands_executor::persisted() {
        return persisted_ == head_;
    }

}
