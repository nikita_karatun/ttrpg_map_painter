#pragma once

#include <QMainWindow>
#include <QDockWidget>

#include <QFile>

#include "map_widget.h"

class QLabel;
class QPushButton;

namespace Ttrpg_map_painter {

    class Main_window;
    class Map_widget;
    class Application_context;
    class Commands_executor;

    class Tools_dock_widget : public QDockWidget {
        private:
            Application_context& context_;
            QPushButton *wall_button_;
            QPushButton *erase_button_;
        protected:
            void showEvent(QShowEvent *) override;
            void closeEvent(QCloseEvent *) override;
        public:
            Tools_dock_widget(Main_window *, Application_context&);
            void set_paint_tool(Paint_tool tool);
    };

    class Main_window : public QMainWindow {
        Q_OBJECT

        private:
            QFile opened_file_;

            Tools_dock_widget *tools_dock_widget_;
            Map_widget *map_widget_;
            QLabel * map_dimentions_label_;

            Application_context& context_;
            Commands_executor& commands_executor_;

            void set_opened_file(const QString& name);

            void create_new_map();
            void open_map();
            void save_map(bool save_as = false);
            void export_map();
            void undo();
            void redo();
        public:
            Main_window(Application_context&);

            bool save_or_discard_changes();

            void closeEvent(QCloseEvent *) override;

            void update_title();
            void update_map_dimentions_label();

            void set_paint_tool(Paint_tool tool);

            Map_widget* map_widget() { return map_widget_; }

    };

}
