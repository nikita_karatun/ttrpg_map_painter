#pragma once

#include <memory>
#include <vector>

#include <QPoint>

#include "math.h"

class QPainter;

namespace Ttrpg_map_painter {

    class Map_widget;
    class Limits;

    class Arrow {
        private:
            std::vector<QLine> lines_;
            bool is_null_;
        public:
            Arrow();
            void set_position(QPoint center, float angle);
            void draw(QPainter& painter);
            bool is_null() { return is_null_; }
            void set_null() { is_null_ = true; } 
    };

    class Arrow_position {
        private:
            struct Calculator {
                Linear_equation::Type type_;
                Map_widget *map_widget_;

                QPoint arrow_center_;
                Calculator(Linear_equation::Type type, Map_widget *map_widget) :type_(type), map_widget_(map_widget) {}

                virtual ~Calculator() {}
                virtual bool outsize_view(const Limits&) = 0;
                virtual bool do_calculate(const Linear_equation& lin_eq) = 0;

                bool calculate(const Limits& limits, const Linear_equation& lin_eq) {
                    if (lin_eq.type() != type_ && outsize_view(limits))
                        return do_calculate(lin_eq);
                    return false; 
                }
            };

            Map_widget *map_widget_;
            std::vector<std::unique_ptr<Calculator>> calculators_;
        public:
            Arrow arrow_;
            Arrow_position(Map_widget *);
            void update();
    };

}
