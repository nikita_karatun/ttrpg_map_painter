#pragma once

#include <QMouseEvent>

class QPainter;

namespace Ttrpg_map_painter {

    class Map_widget;

    class Mouse_state {
        protected:
            Map_widget *map_widget_;
        public:
            Mouse_state(Map_widget *);
            virtual ~Mouse_state() {}
            virtual void on_paint(QPainter&) {}
            virtual void button_released(QMouseEvent *) {}
            virtual QPoint calculate_nearest_point(QPoint);
    };

    class Default_mouse_state : public Mouse_state {
        public:
            Default_mouse_state(Map_widget *);
            void on_paint(QPainter&) override;
            void button_released(QMouseEvent *) override;
    };

    class Left_pressed_mouse_state : public Mouse_state {
        public:
            Left_pressed_mouse_state(Map_widget *);
            void on_paint(QPainter&) override;
            void button_released(QMouseEvent *) override;
            QPoint calculate_nearest_point(QPoint);
    };

}
