#include "mainwindow.h"

#include <QLayout>
#include <QPushButton>
#include <QAction>
#include <QShortcut>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QMenuBar>
#include <QStatusBar>

#include <QMessageBox>
#include <QFileDialog>

#include <QSvgGenerator>

#include "map_io.h"
#include "context.h"
#include "commands.h"

#include "settings_dialog.h"

namespace Ttrpg_map_painter {

    // TODO localization 

    const QKeySequence WALL_TOOL_SHORTCUT(Qt::CTRL + Qt::SHIFT + Qt::Key_W);
    const QKeySequence ERASER_TOOL_SHORTCUT(Qt::CTRL + Qt::SHIFT + Qt::Key_E);

    QString save_file_name_with_retry(QWidget *parent, const QString& title, const Map_io::Supported_file_types& file_types) {
        QString name;
        for (bool repeat = true; repeat; ) {
            repeat = false;
            name = QFileDialog::getSaveFileName(parent, title, {}, file_types.file_dialog_filter_str());
            if (!name.isNull()) {
                auto ext = QFileInfo(QFile(name)).completeSuffix();
                if (ext.isNull()) {
                    QMessageBox::warning(parent, "Warning", "Please specify file name with extension.");
                    repeat = true;
                } else if (!file_types.contains(ext)) {
                    QMessageBox::warning(parent, "Warning", "Unsupported file type.");
                    repeat = true;
                }
            }
        }
        return name;
    }

    class Command_listener : public Commands_executor::Listener {
        public:
            Main_window *main_window_;
            Command_listener(Main_window *main_window) :main_window_(main_window) {}
            void command_executed() override { 
                main_window_->update_title();
                main_window_->update_map_dimentions_label();
            }
            void command_undone() override { 
                main_window_-> update_title();
                main_window_->update_map_dimentions_label();
            }
            void command_redone() override { 
                main_window_-> update_title();
                main_window_->update_map_dimentions_label();
            }
    };

    Tools_dock_widget::Tools_dock_widget(Main_window *main_window, Application_context& context) 
        : QDockWidget(tr("Tools"), main_window), context_(context)
    {
        setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);

        auto root = new QWidget(this);

        wall_button_ = new QPushButton("Wall", root);
        wall_button_->setStatusTip("Draw Wall (" + WALL_TOOL_SHORTCUT.toString() + ")");

        erase_button_ = new QPushButton("Erase", root);
        erase_button_->setStatusTip("Erase Wall (" + ERASER_TOOL_SHORTCUT.toString() + ")");

        auto layout = new QVBoxLayout;
        layout->addWidget(wall_button_);
        layout->addWidget(erase_button_);
        layout->addStretch();
        root->setLayout(layout);
        setWidget(root);

        wall_button_->setEnabled(false);

        connect(wall_button_, &QAbstractButton::clicked, [=]() {
            main_window->set_paint_tool(Paint_tool::Wall);
        });
        connect(erase_button_, &QAbstractButton::clicked, [=]() {
            main_window->set_paint_tool(Paint_tool::Eraser);
        });
    }

    void Tools_dock_widget::showEvent(QShowEvent *) {
        context_.settings().ui_settings().set_tools_enabled(true);
    }

    void Tools_dock_widget::closeEvent(QCloseEvent *) {
        context_.settings().ui_settings().set_tools_enabled(false);
    }

    void Tools_dock_widget::set_paint_tool(Paint_tool tool) {
        bool wall = tool == Paint_tool::Wall;
        wall_button_->setEnabled(!wall);
        erase_button_->setEnabled(wall);
    }

    Main_window::Main_window(Application_context& context) 
        : QMainWindow(nullptr), context_(context), commands_executor_(context.commands_executor())
    {
        set_opened_file("");
        resize(800, 600);
        setLayoutDirection(Qt::LeftToRight);

        commands_executor_.emplace_listener<Command_listener>(this);
        map_widget_ = new Map_widget(this, context);

        tools_dock_widget_ = new Tools_dock_widget(this, context_);
        if (!context_.settings().ui_settings().tools_enabled())
            tools_dock_widget_->close();
        addDockWidget(Qt::RightDockWidgetArea, tools_dock_widget_);

        auto new_action = new QAction(tr("&New"), this);
        new_action->setShortcuts(QKeySequence::New);
        new_action->setStatusTip(tr("Create new map"));
        connect(new_action, &QAction::triggered, this, &Main_window::create_new_map);

        auto open_action = new QAction(tr("&Open..."), this);
        open_action->setShortcuts(QKeySequence::Open);
        open_action->setStatusTip(tr("Open an existing map"));
        connect(open_action, &QAction::triggered, this, &Main_window::open_map);

        auto save_action = new QAction(tr("&Save"), this);
        save_action->setShortcuts(QKeySequence::Save);
        save_action->setStatusTip(tr("Save the map to disk"));
        connect(save_action, &QAction::triggered, this, &Main_window::save_map);

        auto save_as_action = new QAction(tr("Save &As..."), this);
        save_as_action->setShortcuts({QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S)});
        save_as_action->setStatusTip(tr("Save the map to disk"));
        connect(save_as_action, &QAction::triggered, [this] { save_map(true); });

        auto export_action = new QAction(tr("&Export"), this);
        export_action->setShortcuts({QKeySequence(tr("Ctrl+E"))});
        export_action->setStatusTip(tr("Export to image."));
        connect(export_action, &QAction::triggered, this, &Main_window::export_map);

        auto *settings_dialog = new Settings_dialog(this, context_);
        auto settings_action = new QAction(tr("Settings"), this);
        connect(settings_action, &QAction::triggered, [settings_dialog] { settings_dialog->exec(); });

        auto exit_action = new QAction(tr("E&xit"), this);
        exit_action->setShortcuts(QKeySequence::Quit);
        exit_action->setStatusTip(tr("Exit the application"));
        connect(exit_action, &QAction::triggered, this, &QWidget::close); // TODO confirm discard changes message

        auto file_menu = menuBar()->addMenu(tr("&File"));
        file_menu->addAction(new_action);
        file_menu->addAction(open_action);
        file_menu->addAction(save_action);
        file_menu->addAction(save_as_action);
        file_menu->addAction(export_action);
        file_menu->addAction(settings_action);
        file_menu->addAction(exit_action);

        auto undo_action = new QAction(tr("&Undo"), this);
        undo_action->setShortcuts(QKeySequence::Undo);
        connect(undo_action, &QAction::triggered, this, &Main_window::undo);

        auto redo_action = new QAction(tr("&Redo"), this);
        redo_action->setShortcuts({QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_Z)});
        connect(redo_action, &QAction::triggered, this, &Main_window::redo);

        auto edit_menu = menuBar()->addMenu(tr("&Edit"));
        edit_menu->addAction(undo_action);
        edit_menu->addAction(redo_action);

        auto view_menu = menuBar()->addMenu(tr("&View"));
        view_menu->addAction(tools_dock_widget_->toggleViewAction());

        connect(new QShortcut(WALL_TOOL_SHORTCUT, this), &QShortcut::activated, [this]() {
            set_paint_tool(Paint_tool::Wall);
        });
        connect(new QShortcut(ERASER_TOOL_SHORTCUT, this), &QShortcut::activated, [this]() {
            set_paint_tool(Paint_tool::Eraser);
        });

        auto root = new QWidget(this);
        setCentralWidget(root);

        (new QVBoxLayout(root))->addWidget(map_widget_);

        auto status_bar = new QStatusBar(this);
        setStatusBar(status_bar);

        map_dimentions_label_ = new QLabel("", this);
        status_bar->addPermanentWidget(map_dimentions_label_);

        update_map_dimentions_label();

    }

    bool Main_window::save_or_discard_changes() {
        bool confirmed = commands_executor_.persisted();
        if (!confirmed) {
            QMessageBox box;
            box.setWindowTitle("Save the map?");
            box.setText("The map has been modified.");
            box.setInformativeText("Do you want to save your changes?");
            box.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
            box.setDefaultButton(QMessageBox::Save);
            int button = box.exec();
            if (button == QMessageBox::Save)
                save_map();
            if (button == QMessageBox::Save || button == QMessageBox::Discard)
                confirmed = true;
        }
        return confirmed;
    }

    void Main_window::closeEvent(QCloseEvent *event) {
        if (save_or_discard_changes())
            QMainWindow::closeEvent(event);
    }

    void Main_window::set_opened_file(const QString& name) {
        opened_file_.setFileName(name);
        update_title();
    }

    void Main_window::update_title() {
        auto display_name = opened_file_.fileName().isEmpty() ? "new file" : QFileInfo(opened_file_).fileName();
        if (!commands_executor_.persisted())
            display_name += "*";
        setWindowTitle(QString("TTRPG Map Painter") + " - " + display_name);
    }

    void Main_window::update_map_dimentions_label() {
        // TODO check out additional standard font symbols
        auto& limits = context_.map().limits();
        map_dimentions_label_->setText(QString() + QString::number(limits.width()) + " x " + QString::number(limits.height()));
    }

    void Main_window::create_new_map() {
        if (save_or_discard_changes()) {
            set_opened_file("");
            context_.map() = {};
            map_widget_->update();
        }
    }

    void Main_window::open_map() {
        // TODO hadle corrupted file case
        // TODO disable the canvas while opening 
        auto name = QFileDialog::getOpenFileName(this, {}, {}, "*.json");
        if (name.isNull()) 
            return;
        set_opened_file(name);
        opened_file_.open(QIODevice::ReadOnly);
        context_.map() = Map_io::from_json(QJsonDocument::fromJson(opened_file_.readAll()));
        opened_file_.close();
        update_map_dimentions_label();
    }

    void Main_window::save_map(bool save_as) {
        auto& map = context_.map();
        if (map.empty()) {
            QMessageBox::warning(this, "Warning", "Map is empty.");
            return;
        }
        if (save_as || opened_file_.fileName().isEmpty()) {
            auto name = save_file_name_with_retry(this, "Save map", context_.map_file_type());
            if (name.isNull()) 
                return;
            // TODO handle io errors?
            set_opened_file(name);
        }
        opened_file_.open(QIODevice::WriteOnly);
        opened_file_.write(Map_io::to_json(map).toJson());
        opened_file_.close();
        commands_executor_.set_persisted();
        update_title();
    }

    void Main_window::export_map() {
        auto& map = context_.map();
        if (map.empty()) {
            QMessageBox::warning(this, "Warning", "Map is empty.");
            return;
        }

        QString name = save_file_name_with_retry(this, "Export to JPEG file.", context_.supported_export_types());

        if (name.isNull())
            return;

        QFile file(name);
        auto ext = QFileInfo(file).completeSuffix().toLower();
        if (ext == "svg") {
            QSvgGenerator svg;
            svg.setOutputDevice(&file);
            Map_io::write(context_.map_painter(), svg);
        } else
            Map_io::create_image(context_.map_painter(), ext == "png").save(&file);
    }

    void Main_window::undo() {
        if (commands_executor_.has_undo_items()) {
            commands_executor_.undo();
            map_widget_->update();
        }
    }

    void Main_window::redo() {
        if (commands_executor_.has_redo_items()) {
            commands_executor_.redo();
            map_widget_->update();
        }
    }

    void Main_window::set_paint_tool(Paint_tool tool) {
        tools_dock_widget_->set_paint_tool(tool);
        map_widget_->set_pain_tool(tool);
        map_widget_->update();
    }

}
