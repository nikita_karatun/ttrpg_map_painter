#include <iostream>

#include <QString>
#include <QtTest>

#include "map.h"

using namespace Ttrpg_map_painter;

class MapTest : public QObject {
    Q_OBJECT

    private Q_SLOTS:
        void insert_order_test();
        void merge_intervals_test();
        void merge_lines_test();
        void erase_intervals_test();
        void erase_lines_test();
        void limits_test();
};

const std::vector<QLine>& add(Map& map, const QLine& line) {
    map.add(line);
    return map.affected_lines();
}

void MapTest::insert_order_test() {
    Map m1;
    m1.add({{0, 5}, {10, 5}});
    m1.add({{20, 5}, {30, 5}});
    m1.add({{25, 5}, {35, 5}});

    Map m2;
    m2.add({{0, 5}, {10, 5}});
    m2.add({{20, 5}, {30, 5}});
    m2.add({{25, 5}, {35, 5}});
    QVERIFY2(m1 == m2, "maps should be equal");

    m2.add({{35, 5}, {45, 5}});
    QVERIFY2(m1 != m2, "maps should not be equal");
}

struct H_line {
    int y_;
    H_line(int y) :y_(y) {}
    QLine create(int start, int end) { return {start, y_, end, y_}; }
    std::vector<QLine> create(std::initializer_list<std::pair<int, int>> pairs) {
        std::vector<QLine> v;
        v.reserve(pairs.size());
        for (auto& pair : pairs)
            v.push_back(create(pair.first, pair.second));
        return v;
    }
};

struct V_line {
    int x_;
    V_line(int x) :x_(x) {}
    QLine create(int start, int end) { return {x_, start, x_, end}; }
};

void MapTest::merge_intervals_test() {
    QCOMPARE(Interval(0, 10), Interval(10, 0));

    Interval i1(0, 10);
    Interval i2(15, 30);

    auto i1_copy = i1;
    auto i2_copy = i2;
    QVERIFY(!i1.try_merge(i2));
    QCOMPARE(i1, i1_copy);
    QCOMPARE(i2, i2_copy);

    i2 = {5, 15};
    i2_copy = i2;
    QVERIFY(i1.try_merge(i2));
    QCOMPARE(i1, Interval(0, 15));
    QCOMPARE(i2, i2_copy);

    i1 = {15, 5};
    i2 = {10, 0};
    i2_copy = i2;
    QVERIFY(i1.try_merge(i2));
    QCOMPARE(i1, Interval(0, 15));
    QCOMPARE(i2, i2_copy);

    i1 = {0, 10};
    i2 = {5, 7};
    i1_copy = i1;
    i2_copy = i2;
    QVERIFY(i1.try_merge(i2));
    QCOMPARE(i1, i1_copy);
    QCOMPARE(i2, i2_copy);

    i1 = {4, 10};
    i2 = {4, 10};
    i1_copy = i1;
    i2_copy = i2;
    QVERIFY(i1.try_merge(i2));
    QCOMPARE(i1, i1_copy);
    QCOMPARE(i2, i2_copy);

    i2 = {1, 20};
    i2_copy = i2;
    i1.try_merge(i2);
    QVERIFY(i1.try_merge(i2));
    QCOMPARE(i1, i2_copy);
    QCOMPARE(i2, i2_copy);

    i1 = {6, 7};
    i2 = {4, 7};
    QVERIFY(i1.try_merge(i2));
    QCOMPARE(i1, i2);
}

void MapTest::merge_lines_test() {
    Map map;
    H_line h_line(5);

    using lines_type = std::vector<QLine>;

    map.add(h_line.create(0, 10));
    QCOMPARE(to_lines(map), lines_type({h_line.create(0, 10)}));

    map.add(h_line.create(5, 15));
    QCOMPARE(to_lines(map), lines_type({h_line.create(0, 15)}));

    map.add(h_line.create(20, 30));
    QCOMPARE(to_lines(map), h_line.create({{0, 15}, {20, 30}}));

    map.add(h_line.create(-10, -5));
    QCOMPARE(to_lines(map), h_line.create({{-10, -5}, {0, 15}, {20, 30}}));

    map.add(h_line.create(-10, -5));
    QCOMPARE(to_lines(map), h_line.create({{-10, -5}, {0, 15}, {20, 30}}));

    QCOMPARE(add(map, h_line.create(21, 16)), std::vector<QLine>({{20, 5, 30, 5}}));
    QCOMPARE(to_lines(map), h_line.create({{-10, -5}, {0, 15}, {16, 30}}));

    map = {};
    
    map.add(h_line.create(6, 7));
    map.add(h_line.create(4, 7));
    QCOMPARE(to_lines(map), lines_type({h_line.create(4, 7)}));
}

void MapTest::erase_intervals_test() {
    QCOMPARE(Interval(1, 5).erase_or_split({5, 9}), std::make_pair(Interval(1, 5), Interval::EMPTY));
    QCOMPARE(Interval(1, 5).erase_or_split({-5, 1}), std::make_pair(Interval(1, 5), Interval::EMPTY));

    QCOMPARE(Interval(1, 5).erase_or_split({3, 8}), std::make_pair(Interval(1, 3), Interval::EMPTY));
    QCOMPARE(Interval(1, 5).erase_or_split({3, 5}), std::make_pair(Interval(1, 3), Interval::EMPTY));

    QCOMPARE(Interval(1, 5).erase_or_split({-2, 2}), std::make_pair(Interval(2, 5), Interval::EMPTY));

    QCOMPARE(Interval(1, 5).erase_or_split({-2, 7}), std::make_pair(Interval::EMPTY, Interval::EMPTY));
    QCOMPARE(Interval(1, 5).erase_or_split({1, 5}), std::make_pair(Interval::EMPTY, Interval::EMPTY));

    QCOMPARE(Interval(1, 5).erase_or_split({2, 3}), std::make_pair(Interval(1, 2), Interval(3, 5)));

    QCOMPARE(Interval(16, 22).erase_or_split({4, 20}), std::make_pair(Interval(20, 22), Interval::EMPTY));
}

const std::vector<QLine>& remove(Map& map, const QLine& line) {
    map.remove(line);
    return map.affected_lines();
}

void MapTest::erase_lines_test() {
    Map map;
    H_line h_line(5);
    map.add(h_line.create(0, 10));
    map.add(h_line.create(12, 15));
    map.add(h_line.create(16, 22));
    map.add(h_line.create(30, 40));

    map.remove(h_line.create(4, 20));
    QCOMPARE(to_lines(map), h_line.create({{0, 4}, {20, 22}, {30, 40}}));

    QCOMPARE(remove(map, h_line.create(33, 38)), std::vector<QLine>({{30, 5, 40, 5}}));
    QCOMPARE(to_lines(map), h_line.create({{0, 4}, {20, 22}, {30, 33}, {38, 40}}));

    map = {};

    map.add(h_line.create(4, 6));
    map.remove(h_line.create(3, 5));
    QCOMPARE(to_lines(map), std::vector<QLine>({h_line.create(5, 6)}));
}

void MapTest::limits_test() {
    Map map;
    map.add(H_line(5).create(0, 10));
    map.add(H_line(15).create(0, 15));
    map.add(V_line(30).create(0, 10));
    map.update_limits();

    QCOMPARE(map.limits(), Limits(0, 30, 0, 15));
}

QTEST_APPLESS_MAIN(MapTest)

#include "tst_maptest.moc"
