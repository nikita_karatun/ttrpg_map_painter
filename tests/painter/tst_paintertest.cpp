#include <iostream>

#include <QString>
#include <QtTest>

#include <QPainter>
#include <QImage>
#include <QSvgGenerator>

#include "context.h"
#include "map.h"
#include "map_io.h"

// TODO rename test and test project

using namespace Ttrpg_map_painter;

class PainterTest : public QObject
{
    Q_OBJECT

    private:
        static Map& expected_map() {
            auto build_map = [] {
                static Map map;
                map.add({{1, 10}, {1, 15}});
                map.add({{5, 10}, {5, 15}});
                map.add({{5, 10}, {5, 15}});
                return map;
            };
            static Map map = build_map();
            return map;
        }

    private Q_SLOTS:
        void supported_types();
        void read_json_map();
        void write_json_map();
};

QString to_string(QJsonDocument doc) {
    return QTextCodec::codecForName("UTF-8")->toUnicode(doc.toJson());
}

const auto EXPECTED_JSON_STR = QString(R"({
    "walls": [
        {
            "x1": 1,
            "x2": 1,
            "y1": 10,
            "y2": 15
        },
        {
            "x1": 5,
            "x2": 5,
            "y1": 10,
            "y2": 15
        }
    ]
}
)");

void PainterTest::supported_types() {
    Map_io::Supported_file_types fts = {{"Image Files", {"jpg", "png", "bmp"}}, {"SVG", {"svg"}}};
    QCOMPARE(QString("Image Files (*.jpg *.png *.bmp);;SVG (*.svg)"), fts.file_dialog_filter_str());
    QVERIFY(fts.contains("jpg"));
    QVERIFY(!fts.contains("txt"));
}

void PainterTest::read_json_map() {
    auto doc = Map_io::to_json(expected_map());
    auto json_str = to_string(doc);
    QCOMPARE(json_str, EXPECTED_JSON_STR);

    // TODO remove snippet
    // QFile file("../house.json");
    // file.open(QIODevice::ReadOnly);
    // auto map = Map_io::from_json(QJsonDocument::fromJson(file.readAll()));
    // file.close();

    // Settings settings;
    // settings.export_settings().set_bg_enabled(false);
    // std::unique_ptr<Standard_map_painter> map_painter = Map_io::create_image_painter(settings, map);

    // QFile image_file("test.jpg");

    // auto image = Map_io::create_image(map_painter.get(), QFileInfo(image_file).completeSuffix().toLower() == "png");
    // image.save(&image_file);

    // TODO delete snippet
    // QFile svg_file("test.svg");

    // QSvgGenerator svg;
    // svg.setOutputDevice(&svg_file);
    // Map_io::write(map_painter.get(), svg);
}

void PainterTest::write_json_map() {
    auto byte_array = QTextCodec::codecForName("UTF-8")->fromUnicode(EXPECTED_JSON_STR);
    auto doc = QJsonDocument::fromJson(byte_array);
    auto map = Map_io::from_json(doc);
    QCOMPARE(map, expected_map());
}

QTEST_APPLESS_MAIN(PainterTest)

#include "tst_paintertest.moc"
