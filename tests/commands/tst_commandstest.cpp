#include <QString>
#include <QtTest>

#include <memory>
#include <iostream>
#include <sstream>

#include "commands.h"

using namespace Ttrpg_map_painter;

class CommandsTest : public QObject {
    Q_OBJECT

    private Q_SLOTS:
        void test_undo_all();
        void test_size_limit();
        void test_undo_redo();
        void test_persisted();
};

struct Test_command : public Command {
    std::ostream& stream_;
    int id_;
    Test_command(std::ostream& stream, int id) :stream_(stream), id_(id) {}
    void execute() override {
        stream_ << "do(" << id_ << "); ";
    }
    void un_execute() override {
        stream_ << "undo(" << id_ << "); ";
    }
};

struct Commands_generator {
    std::ostream& stream_;
    int id_ = 0;
    Commands_generator(std::ostream& stream) :stream_(stream) {}
    Command* generate() {
        return new Test_command(stream_, ++id_);
    }
};

template<typename F>
void repeat(int count, F f) {
    for (int i = 0; i < count; ++i)
        f(i);
}

void clear(std::stringstream& ss) {
    ss = std::stringstream();
}

void CommandsTest::test_undo_all() {
    std::stringstream ss;
    Commands_generator generator(ss);
    Commands_executor executor(10);
    QVERIFY(!executor.has_undo_items());
    QVERIFY(!executor.has_redo_items());

    repeat(3, [&executor, &generator] (int) { executor.execute(generator.generate()); });
    QCOMPARE(QString(ss.str().c_str()), QString("do(1); do(2); do(3); "));
    QVERIFY(executor.has_undo_items());
    QVERIFY(!executor.has_redo_items());

    clear(ss);
    repeat(3, [&executor] (int) { executor.undo(); });
    QCOMPARE(QString(ss.str().c_str()), QString("undo(3); undo(2); undo(1); "));
    QVERIFY(!executor.has_undo_items());
    QVERIFY(executor.has_redo_items());

    clear(ss);
    repeat(3, [&executor] (int) { executor.redo(); });
    QCOMPARE(QString(ss.str().c_str()), QString("do(1); do(2); do(3); "));
    QVERIFY(executor.has_undo_items());
    QVERIFY(!executor.has_redo_items());

    clear(ss);
    executor.redo();
    QVERIFY(ss.str().empty());
    QVERIFY(executor.has_undo_items());
    QVERIFY(!executor.has_redo_items());
}

void CommandsTest::test_size_limit() {
    size_t expected_count = 3;
    std::stringstream ss;
    Commands_generator generator(ss);
    Commands_executor executor(expected_count);

    repeat(10, [&executor, &generator] (int) { executor.execute(generator.generate()); });
    size_t count = 0;
    while (executor.has_undo_items()) {
        executor.undo();
        ++count;
    }

    QCOMPARE(QString(ss.str().c_str()),
             QString("do(1); do(2); do(3); do(4); do(5); do(6); do(7); do(8); do(9); do(10); undo(10); undo(9); undo(8); "));
    QCOMPARE(count, expected_count);
}

void CommandsTest::test_undo_redo() {
    std::stringstream ss;
    Commands_generator generator(ss);
    Commands_executor executor(5);
    QVERIFY(!executor.has_redo_items());
    QVERIFY(!executor.has_undo_items());

    repeat(5, [&] (int) { executor.execute(generator.generate()); });
    QCOMPARE(QString(ss.str().c_str()), QString("do(1); do(2); do(3); do(4); do(5); "));
    QVERIFY(!executor.has_redo_items());
    QVERIFY(executor.has_undo_items());

    ss = std::stringstream();
    executor.undo();
    QVERIFY(executor.has_redo_items());
    QVERIFY(executor.has_undo_items());

    repeat(3, [&] (int) { executor.undo(); });
    QCOMPARE(QString(ss.str().c_str()), QString("undo(5); undo(4); undo(3); undo(2); "));
    QVERIFY(executor.has_redo_items());
    QVERIFY(executor.has_undo_items());

    ss = std::stringstream();
    repeat(2, [&] (int) { executor.redo(); });
    QCOMPARE(QString(ss.str().c_str()), QString("do(2); do(3); "));

    ss = std::stringstream();
    repeat(2, [&] (int) { executor.execute(generator.generate()); });
    QCOMPARE(QString(ss.str().c_str()), QString("do(6); do(7); "));

    ss = std::stringstream();
    while (executor.has_undo_items())
        executor.undo();
    QCOMPARE(QString(ss.str().c_str()), QString("undo(7); undo(6); undo(3); undo(2); undo(1); "));
}

void CommandsTest::test_persisted() {
    struct Empty_command : public Command {
        void execute() override {}
        void un_execute() override {}
    };

    Commands_executor executor(3);
    QVERIFY(executor.persisted());

    executor.execute<Empty_command>();
    QVERIFY(!executor.persisted());

    executor.undo();
    QVERIFY(executor.persisted());

    repeat(2, [&] (int) { executor.execute<Empty_command>(); });
    QVERIFY(!executor.persisted());

    executor.set_persisted();
    QVERIFY(executor.persisted());

    repeat(4, [&] (int) { 
        executor.execute<Empty_command>();
        QVERIFY(!executor.persisted());
    });

    repeat(4, [&] (int) { executor.execute<Empty_command>(); });
    QVERIFY(!executor.persisted());
}

QTEST_APPLESS_MAIN(CommandsTest)

#include "tst_commandstest.moc"
