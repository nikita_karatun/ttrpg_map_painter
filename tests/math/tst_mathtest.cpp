#include <QString>
#include <QtTest>

#include "math.h"

#include <iostream>

using namespace Ttrpg_map_painter;

class MathTest : public QObject {
    Q_OBJECT

    private Q_SLOTS:
        void testCase1();
};

void MathTest::testCase1() {
    QCOMPARE(calculate_first_non_negative(30, 13), 13);
    QCOMPARE(calculate_first_non_negative(30, 103), 13);
    QCOMPARE(calculate_first_non_negative(30, -13), 17);
    QCOMPARE(calculate_first_non_negative(30, 0), 0);

    QCOMPARE(calculate_nearest(230, 30, 14), 224);
    QCOMPARE(calculate_nearest(230, 30, -14), 226);

    QCOMPARE(calculate_first_grid_value(30, 114), 4);
    QCOMPARE(calculate_first_grid_value(30, -230), -7);

    QCOMPARE(calculate_min(1, 2, 3), 1);
    QCOMPARE(calculate_max(1, 2, 3), 3);

}

QTEST_APPLESS_MAIN(MathTest)

#include "tst_mathtest.moc"
